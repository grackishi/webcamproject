#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		// This will grab video from webcam
		ofVideoGrabber cam;

		// Set Up Frame
		ofImage frame;

		// Set Up GUI
		ofxPanel gui;
		ofxFloatSlider intSlider;
		ofxToggle toggle;
		ofxToggle random;

		
		
};
