#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetBackgroundColor(0, 0, 0, 0);
	cam.setup(1920, 1080);

	//Setup the frame - similar to array, allocate memory
	frame.allocate(1920, 1080, OF_IMAGE_COLOR);

	//Setup GUI 
	gui.setup();
	gui.add(intSlider.setup("Size of colors", 1.5, .75, 2));
	gui.add(toggle.setup("Change Colors", false));
	gui.add(random.setup("Randomize Size", false));


}

//--------------------------------------------------------------
void ofApp::update(){
	// Initialize camera
	if (cam.isInitialized()) {
		cam.update();
		if (cam.isFrameNew()) {
			frame.setFromPixels(cam.getPixels());
		}

	}
}


//--------------------------------------------------------------
void ofApp::draw() {
	//cam.draw(0, 0);
	frame.draw(0, 0);
	
	// Go through and draw an ellipse
	for (int x = 0; x < 1920; x += 20) {
		for (int y = 0; y < 1080; y += 20) {
			
			// Get RGB values from the specified pixel
			float red = frame.getColor(x, y).r;
			float green = frame.getColor(x, y).g;
			float blue = frame.getColor(x, y).b;
			float bright = frame.getColor(x, y).getBrightness();

			//If the toggle for different color is not on, draw in RGB
			if (!toggle) {
				if (red > green && red > blue) {
					ofSetColor(ofColor(255, 0, 0, 1));
					// If random is on, set radius of circles to random number, else set radius based on brightness
					if (!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
					else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
				else if (green > red && green > blue) {
					ofSetColor(ofColor(0, 255, 0, 1));
					if (!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
					else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
				else {
					ofSetColor(ofColor(0, 0, 255, 1));
					if (!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
					else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
			}
			//if the color toggle is on then draw circles in different colors
			else {
				if (red > green && red > blue) {
					ofSetColor(ofColor(245, 66, 182, 1));
					if(!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
				else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
				else if (green > red && green > blue) {
					ofSetColor(ofColor(245, 233, 66, 1));
					if (!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
					else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
				else {
					ofSetColor(ofColor(66, 245, 236, 1));
					if (!random) {
						ofDrawCircle(x, y, bright / intSlider, bright / intSlider);
					}
					else {
						ofDrawCircle(x, y, ofRandom(100, 150) / intSlider, ofRandom(100, 150) / intSlider);
					}
				}
			}	
		}
	}
	
	gui.draw();
}
