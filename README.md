#Assignment 3

![](/screenshot.jpg)


This project uses the webcam to detect pixel values of red, green, and blue. The pixels change to the color
that is most dominant in the pixel. Depending on the brightness of the pixel, the colored shape will change 
size. There are also 3 GUI elements. One that changes the size of the colored shapes, one that randomizes
the size of the colored shapes, and a button that changes the colors that are shown. 